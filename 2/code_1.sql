-- Create DB
use Master;
if DB_ID(N'Bank')is not null
drop database [Bank];
go
create database [Bank]
  on primary
  (name = N'Bank',
  filename = N'C:\db\Bank.mdf',
  size = 51200KB,
  filegrowth = 10240KB)
  log on
  (name = N'Bank_log',
  filename =N'C:\db\Bank_log.ldf',
  size = 10240KB,
  filegrowth = 10%)
  collate Cyrillic_General_100_CI_AI
go
alter database [Bank] set recovery simple with no_wait;
alter database [Bank] set auto_shrink off;
-- TransactionsFG
-- 1
go
alter database [Bank] add filegroup [TransactionsFG0]
go
alter database [Bank] add file
(name = N'TransactionsFG00',
filename = N'C:\db\Bank_TransactionsFG00.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [TransactionsFG0]
go
alter database [Bank] add file
(name = N'TransactionsFG01',
filename = N'C:\db\Bank_TransactionsFG01.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [TransactionsFG0]
-- 2
go
alter database [Bank] add filegroup [TransactionsFG1]
go
alter database [Bank] add file
(name = N'TransactionsFG10',
filename = N'C:\db\Bank_TransactionsFG10.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [TransactionsFG1]
go
alter database [Bank] add file
(name = N'TransactionsFG11',
filename = N'C:\db\Bank_TransactionsFG11.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [TransactionsFG1]
-- 3
go
alter database [Bank] add filegroup [TransactionsFG2]
go
alter database [Bank] add file
(name = N'TransactionsFG20',
filename = N'C:\db\Bank_TransactionsFG20.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [TransactionsFG2]
go
alter database [Bank] add file
(name = N'TransactionsFG21',
filename = N'C:\db\Bank_TransactionsFG21.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [TransactionsFG2]
-- 4
go
alter database [Bank] add filegroup [TransactionsFG3]
go
alter database [Bank] add file
(name = N'TransactionsFG30',
filename = N'C:\db\Bank_TransactionsFG30.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [TransactionsFG3]
go
alter database [Bank] add file
(name = N'TransactionsFG31',
filename = N'C:\db\Bank_TransactionsFG31.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [TransactionsFG3]
-- TimeFG
go
alter database [Bank] add filegroup [TimeFG]
go
alter database [Bank] add file
(name = N'Time0',
filename = N'C:\db\Bank_Time0.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [TimeFG]
-- BalanceFG
go
alter database [Bank] add filegroup [BalanceFG]
go
alter database [Bank] add file
(name = N'Balance0',
filename = N'C:\db\Bank_Balance0.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [BalanceFG]
-- SGFRFG
go
alter database [Bank] add filegroup [SGFR]
go
alter database [Bank] add file
(name = N'SGFRFG0',
filename = N'C:\db\Bank_SGFRFG0.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [SGFR]
go
alter database [Bank] add file
(name = N'SGFRFG1',
filename = N'C:\db\Bank_SGFRFG1.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [SGFR]
go
alter database [Bank] add file
(name = N'SGFRFG2',
filename = N'C:\db\Bank_SGFRFG2.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [SGFR]
go
alter database [Bank] add file
(name = N'SGFRFG3',
filename = N'C:\db\Bank_SGFRFG3.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [SGFR]
-- SGRRFG
go
alter database [Bank] add filegroup [SGRR]
go
alter database [Bank] add file
(name = N'SGRRFG0',
filename = N'C:\db\Bank_SGRRFG0.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [SGRR]
go
alter database [Bank] add file
(name = N'SGRRFG1',
filename = N'C:\db\Bank_SGRRFG1.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [SGRR]
go
alter database [Bank] add file
(name = N'SGRRFG2',
filename = N'C:\db\Bank_SGRRFG2.ndf',
size = 51200KB,
filegrowth = 10240KB)
to filegroup [SGRR];
