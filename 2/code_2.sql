use [Bank]
-- Partition func
create partition function PartFunctionFTransations (bigint)
as range right for values (0, 10000, 20000);
-- Partition scheme
create partition scheme PartSchemeFTransactions
  as partition PartFunctionFTransations to
  ([TransactionsFG0], [TransactionsFG1], [TransactionsFG2], [TransactionsFG3]);
-- DimTables
-- SGRR
create table DimAddress
  (
    address_id bigint identity(1,1) not null,
    country nvarchar(100) not null,
    city nvarchar(100) not null,
    street nvarchar(100) not null,
    post_code int not null,
    constraint PKDW_0 primary key (address_id)
  )
  on [SGRR];

create table DimOfficer
  (
    officer_id bigint identity(1,1) not null,
    constraint PKDW_1 primary key (officer_id)
  )
  on [SGRR];

create table DimAccountType
  (
    account_type_id bigint identity(1,1) not null,
    constraint PKDW_2 primary key (account_type_id)
  )
  on [SGRR];
-- SGFR

create table DimCustomers
  (
    customer_id bigint identity(1,1) not null,
    first_name nvarchar(100) not null,
    middle_name nvarchar(100) not null,
    last_name nvarchar(100) not null,
    birthday nvarchar(100) not null,
    address_id bigint not null,
    constraint PKDW_4 primary key (customer_id),
    constraint FKDW_3 foreign key (address_id)
    references DimAddress (address_id)
  )
  on [SGFR];

create table DimSuppliers
  (
    supplier_id bigint identity(1,1) not null,
    address_id bigint not null,
    name nvarchar(100) not null,
    country nvarchar(100) not null,
    city nvarchar(100) not null,
    post_code int not null,
    constraint PKDW_5 primary key (supplier_id),
    constraint FKDW_4 foreign key (address_id)
    references DimAddress (address_id)
  )
  on [SGFR];

create table DimServices
  (
    service_id bigint identity(1,1) not null,
    name nvarchar(100) not null,
    category nvarchar(100) not null,
    unit_price decimal(15,2) not null,
    constraint PKDW_6 primary key (service_id)
  )
  on [SGFR];
-- FG
create table DimTime
  (
    time_id bigint identity(1,1) not null,
    _date date not null,
    year nvarchar(100) not null,
    quarter int not null,
    month nvarchar(100) not null,
    day int not null,
    week int not null,
    week_day int not null,
    constraint PKDW_7 primary key (time_id)
  )
  on [TimeFG];

create table DimBalance
  (
    balance_id bigint identity(1,1) not null,
    balance decimal(15,2) not null,
    constraint PKDW_8 primary key (balance_id)
  )
  on [BalanceFG];

  create table DimAccounts
    (
      account_id bigint identity(1,1) not null,
      account_number int not null,
      currency nvarchar(100) not null,
      data_opened date not null,
      data_closed date,
      account_type_id bigint not null,
      officer_id bigint not null,
      balance_id bigint not null,
      state nvarchar(100) not null,
      constraint PKDW_3 primary key (account_id),
      constraint FKDW_0 foreign key (account_type_id)
      references DimAccountType (account_type_id),
      constraint FKDW_1 foreign key (officer_id)
      references DimOfficer (officer_id),
      constraint FKDW_2 foreign key (balance_id)
      references DimBalance (balance_id)
    )
    on [SGFR];
-- FactTables
create table FactTransactions
  (
    id bigint identity(1,1) not null,
    transaction_id bigint not null,
    customer_id bigint not null,
    account_id bigint not null,
    supplier_id bigint not null,
    service_id bigint not null,
    time_id bigint not null,
    account_number int not null,
    currency nvarchar(100) not null,
    balance decimal(15,2) not null,
    quantity int not null,
    total decimal(15,2) not null,
    service_category nvarchar(100) not null,
    country nvarchar(100) not null,
    city nvarchar(100) not null,
    constraint PKDW_9 primary key (id),
    constraint FKDW_5 foreign key (customer_id)
    references DimCustomers (customer_id),
    constraint FKDW_6 foreign key (account_id)
    references DimAccounts (account_id),
    constraint FKDW_7 foreign key (supplier_id)
    references DimSuppliers (supplier_id),
    constraint FKDW_8 foreign key (service_id)
    references DimServices (service_id),
    constraint FKDW_9 foreign key (time_id)
    references DimTime (time_id),
  )
  on PartSchemeFTransactions(id);
