use [Bank]
-- Nonclusterd indexes
create index nci_0
  on [DimCustomers]([address_id])
go
create index nci_1
  on [DimAccounts]([account_type_id], [officer_id], [balance_id])
go
create index nci_3
  on [DimSuppliers]([address_id])
go
create index nci_4
  on [FactTransactions]([transaction_id], [customer_id], [account_id], [supplier_id], [service_id], [time_id]);

-- Filtered nonclusterd index
create index fnci_0
  on [DimBalance]([balance])
  where balance>=0
go
create index fnci_1
  on [DimBalance]([balance])
  where balance<0
go
create index fnci_2
  on [FactTransactions]([balance])
  where balance>=0
  go
create index fnci_3
  on [FactTransactions]([balance])
  where balance<0;

-- Column index
create nonclustered columnstore index cnci_0
  on [DimTime]([_date], [year],[quarter],[month],[day],[week],[week_day])
go
create nonclustered columnstore index cnci_1
  on [FactTransactions]([account_number], [currency],[balance],[quantity],[total],[service_category],[country], [city]);
