use [Bank]
-- Partition archoveFunc
create partition function PartFunctionAFTransations (bigint)
as range right for values (10000);
-- Partition archiveScheme
create partition scheme PartSchemeAFTransactions
  as partition PartFunctionAFTransations to
  ([TransactionsFG0], [TransactionsFG1]);
-- ArchiveFactTables
create table ArchiveFactTransactions
  (
    id bigint identity(1,1) not null,
    transaction_id bigint not null,
    customer_id bigint not null,
    account_id bigint not null,
    supplier_id bigint not null,
    service_id bigint not null,
    time_id bigint not null,
    account_number int not null,
    currency nvarchar(100) not null,
    balance decimal(15,2) not null,
    quantity int not null,
    total decimal(15,2) not null,
    service_category nvarchar(100) not null,
    country nvarchar(100) not null,
    city nvarchar(100) not null,
    constraint PKDW_9 primary key (id),
    constraint FKDW_5 foreign key (customer_id)
    references DimCustomers (customer_id),
    constraint FKDW_6 foreign key (account_id)
    references DimAccounts (account_id),
    constraint FKDW_7 foreign key (supplier_id)
    references DimSuppliers (supplier_id),
    constraint FKDW_5 foreign key (service_id)
    references DimServices (service_id),
    constraint FKDW_5 foreign key (time_id)
    references DimTime (time_id),
  )
  on PartSchemeAFTransactions(id);
-- Sliding window procedure
