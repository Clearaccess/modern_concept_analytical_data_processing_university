-- Columns zip
use [Bank]
go
alter table [FactTransactions] rebuild partition = 1 with(data_compression = page)
alter table [FactTransactions] rebuild partition = 2 with(data_compression = page)
alter table [FactTransactions] rebuild partition = 3 with(data_compression = page)
-- CI zip - not need, because auto and not control operation
