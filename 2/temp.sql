-- Clustered index
create clustered index cl_index
  on [db_name].[table_name]([column_name]);
-- Nonclusterd index
create index cl_index
  on [db_name].[table_name]([column_name]);
-- Filtered nonclusterd index
create index cl_index
  on [db_name].[table_name] ([column_name0]) include ([column_name1], [column_name2])
  where column_name0<100;
-- Column index
create nonclustered columnstore index cl_index
  on [db_name].[table_name] ([column_name], [column_name1], [column_name2]);
-- Partition func
create partition function PartFunctionFTransations (bigint);
-- Partition scheme
create partition scheme PartSchemeFTransactions
  as partition PartFunctionFTransations to
  ([TransactionsFG0], [TransactionsFG1], [TransactionsFG2], [TransactionsFG3]);
